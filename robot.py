import time
import threading
from dataclasses import dataclass

from wxpy import Bot, Group, Friend, MP, User, FRIENDS, embed, Member


@dataclass
class Message:
    content: str
    from_w: User
    to:  User

botControl = True

bot = Bot(console_qr=True)
wanshi = bot.friends().search('顽石')[0]
xiaobing = bot.mps().search('小冰')[0]
lastSender = wanshi
me = bot.file_helper
adminUsers  = [wanshi]

# friends 
@bot.register(bot.friends(update=True))
def print_all_msg(msg):
    global lastSender
    if botControl:
        xiaobing.send(msg.text)
        lastSender = msg.sender
        # print(msg.text, msg.sender, msg.type)

# adminitrator 
@bot.register(adminUsers)
def admin_msg(msg):
    global botControl
    # global lastSender
    if msg.text == '别说话':
        print(':(')
        lastSender.send('不跟你们说了，没意思')
        botControl = False
    elif msg.text == '你自由了':
        lastSender.send('刚才去河口市，现在我又可以说话了')
        botControl = True
        print(':)')
    xiaobing.send(msg.text)
    # lastSender = msg.sender


@bot.register(xiaobing)
def foward_xiaobing(msg):
    # print(lastSender)
    # lastSender.send(msg.text)
    msg.forward(lastSender)


@bot.register(bot.groups(update=True))
def reply_goups_msg(msg):
    global lastSender
    global botControl
    if botControl:
        if msg.is_at:
            # print(msg.member.name, wanshi.name)
            #TODO   make it more smart
            if msg.member.name == wanshi.name and msg.text[len(bot.self.name)+2:]== '别跟他们说话了':
                msg.sender.send('好，不跟他们一般见识, 不说了')
                botControl = False
            else:
                 xiaobing.send(msg.text[len(bot.self.name)+1:])
        else:
            xiaobing.send(msg.text)
        lastSender = msg.sender
    elif msg.member.name == wanshi.name and msg.text[len(bot.self.name)+2:]  == '想说就说吧':
        msg.sender.send('那我开始喷了啊')
        botControl = True
    # print(msg.text, msg.sender, msg.type)


@bot.register(msg_types=FRIENDS)
def auto_accept_friends(msg):
    # 接受好友请求
    new_friend = msg.card.accept()
    # 向新的好友发送消息
    new_friend.send('哈哈，我自动接受了你的好友请求')



def autoSendFunc(target):
    count = 0
    while True:
        for t in target:
            t.send(str(count))
            time.sleep(2)
        time.sleep(60*5)
        count += 1
        if count >= 1000:
            count = 1


class MyThread(threading.Thread):
    def __init__(self, threadID: int, name: str):
        threading.Thread.__init__(self)
        self.target = [me]
        self.threadID = threadID
        self.name = name

    def run(self):
        autoSendFunc(self.target)


if __name__ == '__main__':
    
    thread1 = MyThread(1, 'keepAlive')
    thread1.start() 
    # embed()

    while True:
        # print('hi')
        time.sleep(10)
